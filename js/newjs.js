let changeButton = document.createElement('button');
let ourTheme = document.getElementById('our-style');
let defaultTheme = 'css/style.css';

document.body.appendChild(changeButton);
changeButton.innerText = 'MAKE SOME CHANGE';
changeButton.classList.add('change-button');

window.onload = function () {
    let theme = localStorage.getItem('cssLink');
    ourTheme.setAttribute('href', theme || defaultTheme);

    if (!theme) {
        localStorage.setItem('cssLink', defaultTheme);
    }
};

changeButton.addEventListener('click', () => {
    let theme = localStorage.getItem('cssLink')
    let newTheme = theme === defaultTheme ? 'css/style2.css' : defaultTheme;

    localStorage.setItem('cssLink', newTheme);

    ourTheme.setAttribute('href', newTheme);
});


