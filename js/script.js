let changeButton = document.createElement('button');
let ourTheme = document.getElementById('our-style');

console.log(ourTheme);

document.body.appendChild(changeButton);
changeButton.innerText = 'MAKE SOME CHANGE';
changeButton.classList.add('change-button');


window.onload = function(){
    if (localStorage.getItem('cssLink') === 'css/style.css') {
        ourTheme.setAttribute('href', 'css/style.css');
    }
    else if (localStorage.getItem('cssLink') === 'css/style2.css') {
        ourTheme.setAttribute('href', 'css/style2.css');
    }
};


changeButton.addEventListener('click', () => {
    if (localStorage.getItem('cssLink') === null) {
        ourTheme.setAttribute('href', 'css/style2.css');
        localStorage.setItem('cssLink', 'css/style2.css');
    } else  if (localStorage.getItem('cssLink') === 'css/style.css') {
        ourTheme.setAttribute('href', 'css/style2.css');
        localStorage.setItem('cssLink', 'css/style2.css');
    } else if (localStorage.getItem('cssLink') === 'css/style2.css') {
        ourTheme.setAttribute('href', 'css/style.css');
        localStorage.setItem('cssLink', 'css/style.css')
    }
});
